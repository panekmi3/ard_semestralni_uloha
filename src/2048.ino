#include <SD.h>
#include <TFT.h>
#include <SPI.h>

#define DOWN_BUTTON_PIN 2
#define RIGHT_BUTTON_PIN 3
#define UP_BUTTON_PIN 4
#define LEFT_BUTTON_PIN 5
#define BACK_BUTTON_PIN 7

#define MAX_SIZE 4

#define NONE 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3
#define UP 4
#define BACK 5

TFT display = TFT(10, 9, 8);

//reprezents a single block
struct BLOCK
{
    public:
        char x;
        char y;

        int value;
        bool isNew;
} typedef Block;

//used to store the color of a block
struct COLOR
{
    COLOR(char r, char g, char b)
    {
        red = r;
        green = g;
        blue = b;
    }
    
    COLOR( int value )
    {
        red = value % 2 * 127;
        green = value % 3 * 127;
        blue = value % 5 * 127;
    }

    char red;
    char green;
    char blue;
} typedef Color;

//operators to make it easier to work with the block struct
bool operator == (Block b1, Block b2)
{
    return b1.value == b2.value;
}

bool operator != (Block b1, Block b2)
{
    return b1.value != b2.value;
}

bool operator == (Block b1, int n)
{
    return b1.value == n;
}

bool operator != (Block b1, int n)
{
    return b1.value != n;
}

//used for debugging
const char * DirectionsStr[6] =
{
    "NONE", "DOWN", "RIGHT", "LEFT", "UP", "BACK"
};

//encapsulates all the logic and data needed for the game
class Game
{
    public:
        //creates an instance of the game with the size of gameSize
        Game( int gameSize = 4 )
        {
            this->size = min( gameSize, MAX_SIZE );
            this->score = 0;
            blockSize = display.height() / size - 4;

            int i = 0;

            for (char x = 0; x < size; x++)
            {
                for (char y = 0; y < size; y++)
                {                    
                    field[x][y].x = x;
                    field[x][y].y = y;
                    field[x][y].value = i;
                    field[x][y].isNew = false;
                }
            }

            //used for debugging

            // field[0][0].value = 9;
            // field[3][0].value = 9;
            // field[0][3].value = 9;
            // field[3][3].value = 9;
             

            newTile();
            newTile();
            updateWorkField();            
            updateOldField();
            
            this->initialRender();
            this->render();
        }

        ~Game()
        {

        }

        //used for debugging
        void renderSerial() 
        {
            Serial.print("n");
            for (int y = 0 ; y < size ; y++)
            {
                Serial.print("|----+----+----+----|\n");
                for (int x = 0 ; x < size ; x++)
                {
                    Serial.print("|");

                    int tile = getTile(x, y);
                    if ( tile )
                    {
                        int len = log10(tile) + 1;
                        for (size_t i = 0; i < 4 - len; i++)
                        {
                            Serial.print( " " );
                        }
                        
                        Serial.print( tile );
                    }
                        
                    else
                        Serial.print("    ");

                    if( x == size - 1 )
                        Serial.print("|");
                }
                Serial.print("\n");
            }
            Serial.print("|----+----+----+----|\n");
        }

        //renders the currnet game state to the display
        void render()
        {
            renderSerial();
            
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    int value = getTile(x, y);                                    
                   
                    Color c = Color( value );

                    display.fill(c.red, c.green, c.blue);
                    display.stroke(0, 0, 0);
                    display.rect(x * blockSize + 1, y * blockSize + 1, blockSize - 2, blockSize - 2);
                    display.noFill();
                    display.stroke(0, 0, 0);

                    if( value )
                    {
                        snprintf(str, 6, "%d", value);

                        display.text( str, ( x * blockSize ) + ( blockSize / 2 ) - (int) log10(value) * 2 - 2 , y * blockSize + blockSize / 2  );
                    }
                }
            }

            int scoreOffset = blockSize * size + 5;

            display.fill(0, 0, 0);
            display.stroke(0, 0, 0);
            display.rect(scoreOffset, 10, display.width() - scoreOffset, 10);
            display.noFill();
            display.stroke(255, 255, 255);
            snprintf(str, 6, "%d", score);
            display.text(str, blockSize * size + 5, 10);

            if( checkWin() )
            {
                display.text("You win!", scoreOffset, 20);
            }
            else
            {
                display.fill(0, 0, 0);
                display.stroke(0, 0, 0);
                display.rect(scoreOffset, 20, display.width() - scoreOffset, 10);
                display.noFill();
                display.stroke(255, 255, 255);
            }
            if( ! canMove() )
            {
                display.text("No moves, game over!", 10 , display.height() - 10);
            }
            else
            {
                display.fill(0, 0, 0);
                display.stroke(0, 0, 0);
                display.rect(0, display.height() - 10, display.width(), 10);
                display.noFill();
                display.stroke(255, 255, 255);
            }
        }

        //render the grid and score text, since those dont need to be rendered every move
        void initialRender()
        {
            display.stroke(255, 255, 255);

            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    display.rect( x * blockSize, y * blockSize, blockSize, blockSize);
                }
            }

            display.text("Score:", blockSize * size + 5, 0);
        }

        //add a new tile to a random empty space
        bool newTile()
        {
            randomSeed(millis());

            Block free[MAX_SIZE * MAX_SIZE];
            int number = 0;

            for (int x = 0 ; x < size ; x++)
            {
                for (int y = 0 ; y < size ; y++)
                {
                    if ( getTile(x, y) == 0 )
                    {
                        free[number].x = x;
                        free[number++].y = y;
                    }                    
                }
            }

            if (number == 0)
            {
                return false;
            }
            
            int selected = random(number);

            field[ free[selected].x ] [ free[selected].y ].value = 1;

            score += 1;

            return true;            
        }

        //if posible, relizes a single move
        void move( int dir )
        {
            Serial.println(DirectionsStr[dir]);

            updateWorkField();

            bool moved = false;
            if( dir == BACK )
            {
                back();
                render();
                return;
            }

            if( ! canMove() )
            {
                Serial.println("No possible moves! GameOver");
                render();
                return;
            }

            switch (dir)
            {
            case UP:
                moveUP();
                break;
            case DOWN:
                moveDOWN();
                break;
            case LEFT:
                moveLEFT();
                break;
            case RIGHT:
                moveRIGHT();
                break;
            default:
                break;
            }            

            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    field[x][y].isNew = false;

                    if( field[x][y] != workField[x][y] )
                    {
                        moved = true;
                    }
                }
            }            

            if( moved )
            {
                updateOldField();
                newTile();
                
                render();
            }
            else
            {
                Serial.println("Neplatný pohyb!");
            }
        }

        //returns true, if there is a valid move for the player
        bool canMove()
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if( getTile(x, y) == 0 )
                    {
                        return true;
                    }
                }
            }

            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    if( getTile( x + 1, y ) == getTile( x , y ) ) return true;
                    if( getTile( x - 1, y ) == getTile( x , y ) ) return true;
                    if( getTile( x, y + 1 ) == getTile( x , y ) ) return true;
                    if( getTile( x, y - 1 ) == getTile( x , y ) ) return true;
                }
            }

            return false;
        }

        //returns true if the player has won
        bool checkWin()
        {
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    if( field[x][y] == 11 )
                    {
                        return true;
                    }
                }
                
            }

            return false;            
        }

    private:
        unsigned int size;
        unsigned int score;
        unsigned int blockSize;

        char str[6];
        Block field[ MAX_SIZE ][ MAX_SIZE ];
        Block oldField[ MAX_SIZE ][ MAX_SIZE ];
        Block workField[ MAX_SIZE ][ MAX_SIZE ];

        //returns the value of a single tile, -1 if out of bounds
        int getTile( int x, int y )
        {
            if (x < 0 || x >= size || y < 0 || y >= size)
            {
                return -1;
            }           

            return this->field[x][y].value;
        }

        //swaps the position of two blocks in an array
        void swapBlocks( Block * ptr1, Block * ptr2 )
        {
            Block temp = *ptr1;
            *ptr1 = *ptr2;
            *ptr2 = temp;
        }

        //merges two blocks and updates the score
        void merge( Block * ptr1, Block * ptr2 )
        {
            ptr2->value = 0;
            ptr1->value = ( ( ptr1->value ) + 1 );
            ptr1->isNew = true;

            score += ptr1->value;
        }

        //swaps the contents of the oldField and field arrays
        void back()
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    swapBlocks( & field[x][y], & oldField[x][y] );
                }
            }
        }

        //realizes a single move up
        void moveUP()
        {
            //we iterate from top to prevent the block bumping into a block that would move afterwards
            for( int y = 0 ; y < size ; y ++ )
            {
                for( int x = 0 ; x < size ; x ++ )
                {                        
                    //first iterate through all the blocks and find those that are not zeroes
                    if( field[x][y].value != 0 )
                    {
                        //for all blocks above
                        for ( int r = 0 ; r < y ; r ++ )
                        {
                            //if the block is zero, the space is free, and the block can slide there
                            if( field[x][r].value == 0 )
                            {
                                swapBlocks( & field[x][r], & field[x][y] );
                                break;
                            }

                            //if the block has the same value, it can potentialy be merged
                            else if( ! field[x][r].isNew && field[x][r] == field[x][y] )
                            {
                                bool pathClear = true;
                                //check if the path to the block is clear
                                for ( int i = r + 1 ; i < y ; i++ )
                                {
                                    if( field[x][i] != 0 )
                                    {
                                        pathClear = false;
                                    }
                                }
                                //if yes, merge these blocks
                                if (pathClear)
                                {
                                    merge( & field[x][r], & field[x][y] );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            
        }

        //realizes a single move down
        void moveDOWN()
        {
            for(int y = size - 1 ; y >= 0 ; y --)
            {
                for( int x = 0 ; x < size ; x ++)
                {
                    if( field[x][y] != 0 )
                    {
                        for (int r = size - 1; r > y; r --)
                        {
                            if( field[x][r] == 0 )
                            {
                                swapBlocks( & field[x][r], & field[x][y] );
                                break;
                            }
                            else if( ! field[x][r].isNew && field[x][r] == field[x][y] )
                            {
                                bool pathClear = true;

                                for ( int i = r - 1 ; i > y ; i-- )
                                {
                                    if( field[x][i] != 0 )
                                    {
                                        pathClear = false;
                                    }
                                }

                                if (pathClear)
                                {
                                    merge( & field[x][r], & field[x][y] );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        //realizes a single move left
        void moveLEFT()
        {
            for( int x = 0 ; x < size ; x++ )
            {
                for ( int y = 0 ; y < size ; y++ )
                {
                    if( field[x][y] != 0 )
                    {
                        for ( int r = 0 ; r < x ; r++ )
                        {
                            if( field[r][y] == 0 )
                            {
                                swapBlocks( & field[r][y], & field[x][y] );
                                break;
                            }
                            else if( ! field[r][y].isNew && field[r][y] == field[x][y] )
                            {
                                bool pathClear = true;

                                for ( int i = r + 1 ; i < x - 1 ; i++ )
                                {
                                    if( field[i][y] != 0 )
                                    {
                                        pathClear = false;
                                    }
                                }

                                if (pathClear)
                                {
                                    merge( & field[r][y], & field[x][y] );
                                    break;
                                }
                            }
                        }
                    }
                }
            }            
        }
        //realizes a single move right
        void moveRIGHT()
        {
            for( int x = size - 1 ; x >= 0 ; x-- )
            {
                for ( int y = 0 ; y < size ; y++ )
                {
                    if( field[x][y] != 0 )
                    {
                        for ( int r = size - 1 ; r > x; r-- )
                        {
                            if( field[r][y] == 0 )
                            {
                                swapBlocks( & field[r][y], & field[x][y] );
                                break;
                            }
                            else if( ! field[r][y].isNew && field[r][y] == field[x][y] )
                            {
                                bool pathClear = true;

                                for ( int i = r - 1 ; i > x ; i-- )
                                {
                                    if( field[i][y] != 0 )
                                    {
                                        pathClear = false;
                                    }
                                }

                                if (pathClear)
                                {
                                    merge( & field[r][y], & field[x][y] );
                                    break;
                                }
                            }
                        }
                    }
                }
            }            
        }

        //updates the values of the oldField array
        void updateOldField()
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    oldField[x][y] = workField[x][y];
                }
            }
        }

        //updates the values of the workField array
        void updateWorkField()
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    workField[x][y] = field[x][y];
                }
            }
        }

};

//returns the direction representing a pressed button, 0 if none are pressed
int readButtons()
{
    if ( ! digitalRead(DOWN_BUTTON_PIN)) return DOWN;
    if ( ! digitalRead(RIGHT_BUTTON_PIN)) return RIGHT;
    if ( ! digitalRead(UP_BUTTON_PIN)) return UP;
    if ( ! digitalRead(LEFT_BUTTON_PIN)) return LEFT;
    if ( ! digitalRead(BACK_BUTTON_PIN)) return BACK;
    return 0;
}

int dir, lastDir = 0;
Game game;

void setup()
{
    Serial.begin(9600);

    display.begin();
    display.background(0, 0, 0);

    pinMode( DOWN_BUTTON_PIN, INPUT_PULLUP );
    pinMode( RIGHT_BUTTON_PIN, INPUT_PULLUP );
    pinMode( UP_BUTTON_PIN, INPUT_PULLUP );
    pinMode( LEFT_BUTTON_PIN, INPUT_PULLUP );
    pinMode( BACK_BUTTON_PIN, INPUT_PULLUP );

    game = Game(4);
}

void loop()
{
    dir = readButtons();
    if ( dir != 0 && dir != lastDir)
    {
        delay(5);
        dir = readButtons();

        game.move(dir);  

    }
    lastDir = dir;
}